import os
import random
import time
import sys
import pickle
import json
import boto3
import warnings
from Inspector import *

def local_predict(model_path, review):
    inspector = Inspector()
    inspector.inspectAll()
    with open(model_path, 'rb') as training_model:
        model = pickle.load(training_model)

    class_predicted = (model.predict([review]).tolist())[0]
    pred_probabs = (model.predict_proba([review]).tolist())[0]
    pred_json = json.dumps({'class_predicted': class_predicted, 'predict_probability':pred_probabs})
    inspector.addAttribute("prediction", pred_json)

    inspector.inspectAllDeltas()
    response = inspector.finish()
    return response

def aws_predict(aws_client, review):
    params = dict()
    params['review'] = review
    response = aws_client.invoke(
        FunctionName='pytest',
        InvocationType='RequestResponse',
        LogType='Tail',
        Payload=json.dumps(params))
    return response['Payload'].read().decode('utf-8')

aws_client = boto3.client(
    'lambda',
    region_name='us-east-2',
    aws_access_key_id='AKIAJIBQBKWODFXK3RNA',
    aws_secret_access_key='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')

local_model_path = "./text_classifier"

def run_test(path, method, count, warmup):
    processed_files = set()

    review_class_str = "pos"
    review_class = 1
    new_path = "{}/{}".format(path, review_class_str)

    total_time = 0
    actual_runtime = 0
    correct_predictions = 0
    total_reviews = 0

    for i in range(0, count):
        if i == (count // 2):
            review_class_str = "neg"
            review_class = 0
            new_path = "{}/{}".format(path, review_class_str)
            processed_files = set()
        
        review_file = random.choice(os.listdir(new_path))
        f = os.path.join(new_path, review_file)

        while review_file in processed_files:
            review_file = random.choice(os.listdir(new_path))
            f = os.path.join(new_path, review_file)

        processed_files.add(f)

        with open(f, encoding='utf8') as fp:
            review=fp.readline()

            start = time.time()

            if method == "aws":
                response = aws_predict(aws_client, review)
            else:
                response = local_predict(local_model_path, review)

            total_time += time.time() - start
            j = json.loads(response)

            prediction = j
            prediction = json.loads(j['prediction'])
            actual_runtime += j['frameworkRuntime']    

            total_reviews += 1
            if prediction['class_predicted'] == review_class:
                correct_predictions += 1

    return total_time, (correct_predictions * 100 / total_reviews), actual_runtime

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("USAGE: python nb_test.py <path> <method> <count>")
        sys.exit()
    
    path = sys.argv[1]
    method = sys.argv[2]
    count = int(sys.argv[3])

    run_test(path, method, 2, True)

    total_time, accuracy, actual_runtime = run_test(path, method, count, False)

    print("Total time taken = {} sec.".format(total_time))
    print("Accuracy = {}%.".format(accuracy))
    print("Actual runtime = {} sec.".format(actual_runtime))
    
