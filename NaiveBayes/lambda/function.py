import json
import sys
import boto3
import pickle
from Inspector import *

s3_client = boto3.client('s3')

bucket = 'tcss562-bucket'
key = 'text_classifier'

def handler(event, context):

    review = str(event.get('review'))

    download_path = '/tmp/text_classifier.pkl'
    s3_client.download_file(bucket, key, download_path)

    inspector = Inspector()
    inspector.inspectAll()

    with open(download_path, 'rb') as training_model:
        model = pickle.load(training_model)

    class_predicted = (model.predict([review]).tolist())[0]
    #print("class_predicted : ", class_predicted)

    pred_probabs = (model.predict_proba([review]).tolist())[0]
    #print("pred_probabs : ", pred_probabs)

    pred_json = json.dumps({'class_predicted': class_predicted, 'predict_probability':pred_probabs})
    #print(pred_json)

    inspector.addAttribute("prediction", pred_json)

    inspector.inspectAllDeltas()
    response = inspector.finish()
    print(response)
    return response

if __name__ == "__main__":
    handler(None, None)
